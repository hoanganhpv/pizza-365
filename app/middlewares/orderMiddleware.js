const createAOrderOfUserMiddleware = (req, res, next) => {
    console.log(`Thực hiện xử lý orderMiddleware tại tầng middleware để tiến hành tạo mới 1 order...`);
    next();
};

const getAOrderOfUserMiddleware = (req, res, next) => {
    console.log(`Thực hiện xử lý orderMiddleware tại tầng middleware để tiến hành lấy ra toàn bộ order của 1 user...`);
    next();
};

const getAllOrderMiddleware = (req, res, next) => {
    console.log(`Thực hiện xử lý orderMiddleware tại tầng middleware để tiến hành lấy ra toàn bộ order...`);
    next();
};

const getAOrderOfAUserMiddleware = (req, res, next) => {
    console.log(`Thực hiện xử lý orderMiddleware tại tầng middleware để tiến hành lấy ra 1 order của 1 user chỉ định...`);
    next();
};

const updateAOrderMiddleware = (req, res, next) => {
    console.log(`Thực hiện xử lý orderMiddleware tại tầng middleware để tiến hành cập nhật 1 order chỉ định...`);
    next();
};

const deleteAOrderMiddleware = (req, res, next) => {
    console.log(`Thực hiện xử lý orderMiddleware tại tầng middleware để tiến hành xóa 1 order chỉ định...`);
    next();
};

export {
    createAOrderOfUserMiddleware,
    getAOrderOfUserMiddleware,
    getAllOrderMiddleware,
    getAOrderOfAUserMiddleware,
    updateAOrderMiddleware,
    deleteAOrderMiddleware
};