const getAllUserMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý userMiddleware tại tầng middleware để thực hiện trả về client toàn bộ user...`);
    next();
};

const getAUserByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý userMiddleware tại tầng middleware để thực hiện trả về client 1 user theo id truyền vào...`);
    next();
};

const createAUserMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện tạo mới 1 user...`);
    next();
};

const updateAUserByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện chỉnh sửa 1 user theo id và body...`);
    next();
};

const deleteAUserByIdMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện xóa 1 user theo id truyền vào...`);
    next();
};

const getLimitUserMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện lấy ra 1 số lượng user nhất định...`);
    next();
};

const getSkipUserMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện lấy ra 1 all user và skip đi 1 số lượng user truyền vào từ query...`);
    next();
};

const getSortesUsersMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện lấy ra 1 all user và sắp xếp kết quả trả về theo thứ tự alpha-bet...`);
    next();
};

const getSkipLimitUsersMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện lấy ra 1 all user và sắp xếp kết quả trả limit phần tử đầu tiên tính từ vị trí skip...`);
    next();
};

const getSortedSkipLimitUsersMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý usermiddleware tại tầng middleware để thực hiện lấy ra 1 all user và sắp xếp kết quả trả limit phần tử đầu tiên tính từ vị trí skip và sắp xêps theo thứ tự alphabet...`);
    next();
};

export {
    getAllUserMiddleware,
    getAUserByIdMiddleware,
    createAUserMiddleware,
    updateAUserByIdMiddleware,
    deleteAUserByIdMiddleware,
    getLimitUserMiddleware,
    getSkipUserMiddleware,
    getSortesUsersMiddleware,
    getSkipLimitUsersMiddleware,
    getSortedSkipLimitUsersMiddleware
};