// Xử lý tại tầng middleware trước khi thực hiện các thao tác khác.
const handlingWhenAccessing = (req, res, next) => {
    console.log(`Đang thực hiện xử lý drinkMiddleware tại tầng middleware khi người dùng truy cập đến đường dẫn /drinks...`);
    next();
};

// Xử lý tại tầng middleware trước khi thực hiện hàm lấy ra danh sách nước uống tạị controller
const getAllDrinkMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý drinkmiddleware tại tầng middleware để lấy ra tất cả danh sách nước uống...`);
    next();
};

// Xử lý tại tầng middleware trước khi tiến hành tạo mới 1 loại nước uống
const createADrinkMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý drinkmiddleware tại tầng middleware để tạo mới 1 loại nước uống...`);
    next();
};

// Xử lý tại tầng middleware trước khi tiến hành cập nhật 1 loại nước uống theo id và body truyền vào
const updateADrinkMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý drinkmiddleware tại tầng middleware để cập nhật 1 loại nước uống...`);
    next();
};

// Xử lý tại tầng middleware trước khi tiến hành xóa 1 loại nước uống theo id truyền vào
const deleteADrinkMiddleware = (req, res, next) => {
    console.log(`Đang thực hiện xử lý drinkmiddleware tại tầng middleware để xóa 1 loại nước uống...`);
    next();
};

export {
    handlingWhenAccessing,
    getAllDrinkMiddleware,
    createADrinkMiddleware,
    updateADrinkMiddleware,
    deleteADrinkMiddleware
}