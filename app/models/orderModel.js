import mongoose from "mongoose";
import randToken from 'rand-token'

const Schema = mongoose.Schema;

const orderSchema = Schema({
    _id: {
        type: Schema.Types.ObjectId
    },
    orderCode: {
        type: String,
        unique: true,
        default: () => randToken.generate(8)
    },
    pizzaSize: {
        type: String,
        require: true
    },
    pizzaType: {
        type: String,
        require: true
    },
    voucher: {
        type: Schema.Types.ObjectId,
        ref: 'Drink'
    },
    drink: {
        type: Schema.Types.ObjectId,
        ref: 'Drink'
    },
    status: {
        type: String,
        require: true
    }
}, {
    timestamps: true
});

const Order = mongoose.model("Order", orderSchema);

export { Order };