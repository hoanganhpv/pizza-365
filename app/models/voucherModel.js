import mongoose from "mongoose";

const Schema = mongoose.Schema;

const voucherSchema = Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    maVouCher: {
        type: String,
        unique: true,
        require: true
    },
    phanTramGiamGia: {
        type: Number,
        require: true
    },
    ghiChu: {
        type: String,
        require: false
    }
}, {
    timestamps: true
});

const Voucher = mongoose.model('Voucher', voucherSchema);

export { Voucher };