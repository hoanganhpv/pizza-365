import mongoose from "mongoose";

const Schema = mongoose.Schema;

const drinkSchema = Schema({
    _id: {
        type: Schema.Types.ObjectId
    },
    maNuocUong: {
        type: String,
        unique: true,
        require: true
    },
    tenNuocUong: {
        type: String,
        require: true
    },
    donGia: {
        type: Number,
        require: true
    }
}, {
    timestamps: true
}, {
    timestamps: true
});

const Drink = mongoose.model('Drink', drinkSchema);

export { Drink };