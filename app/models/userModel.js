import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// Khởi tạo Schema User
const userSchema = Schema({
    _id: {
        type: Schema.Types.ObjectId,
        unique: true
    },
    fullName: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    address: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true,
        unique: true
    },
    orders: [{ type: Schema.Types.ObjectId, ref: 'Order' }]
}, {
    timestamps: true
});

const User = mongoose.model('User', userSchema);

export { User };