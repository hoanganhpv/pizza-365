import express from "express";
// IMPORT ORDERMIDDLEWARE
import {
    createAOrderOfUserMiddleware,
    getAOrderOfUserMiddleware,
    getAllOrderMiddleware,
    getAOrderOfAUserMiddleware,
    updateAOrderMiddleware,
    deleteAOrderMiddleware
} from '../middlewares/orderMiddleware.js'
// IMPORT ORDERMIDDLEWARE

// IMPORT CONTROLLER
import {
    createAOrderOfUserController,
    getAllOrderByUserIdController,
    getAllOrderController,
    getAOrderOfUserController,
    updateAOrderController,
    deleteAOrderController
} from '../controller/orderController.js';
// IMPORT CONTROLLER

const orderRouter = express.Router();

// Router thực hiện tạo mới 1 order thuộc users có id user truyền vào hợp lệ 
orderRouter.post('/users/:userId/orders', createAOrderOfUserMiddleware, createAOrderOfUserController);

// Router thực hiện lấy ra toàn bộ order của 1 user truyền vào id dưới dạng tham số trong url
orderRouter.get('/users/:userId/orders', getAOrderOfUserMiddleware, getAllOrderByUserIdController);

// Router thực hiện lấy ra toàn bộ order có trong database
orderRouter.get('/orders', getAllOrderMiddleware, getAllOrderController);

// Router lấy ra 1 order của 1 user 
orderRouter.get('/users/:userId/orders/:orderId', getAOrderOfAUserMiddleware, getAOrderOfUserController);

// Router cập nhật 1 order bất kỳ
orderRouter.put('/orders/:orderId', updateAOrderMiddleware, updateAOrderController);

// Router xóa 1 order
orderRouter.delete('/orders/:orderId', deleteAOrderMiddleware, deleteAOrderController)

export { orderRouter };