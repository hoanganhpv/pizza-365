// Import module express
import express from "express";
// import middleware
import {
    handlingWhenAccessing,
    getAllDrinkMiddleware,
    createADrinkMiddleware,
    updateADrinkMiddleware,
    deleteADrinkMiddleware
} from '../middlewares/drinkMiddleware.js';
// Import controller
import {
    getAllDrinkController,
    createAdrinkController,
    updateADrinkController,
    deleteADrinkController
} from '../controller/drinkController.js'
// Khởi tạo drink router
const drinkRouter = express.Router();

// Xử lý khi client chạm đến đường dẫn /drinks
drinkRouter.use('/', handlingWhenAccessing);

// Xử lý khi người dùng truy cập vào đúng đường dẫn /drinks mà không dùng phương thức nào (mặc định GET)
drinkRouter.get('/', getAllDrinkMiddleware, getAllDrinkController);

// Xử lý khi người dùng truy cập đến /drinks bằng phương thức POST và truyền vào 1 body
drinkRouter.post('/', createADrinkMiddleware, createAdrinkController);

// Xử lý khi người dùng muốn sửa đổi thông tin 1 loại nước uống
drinkRouter.put('/:paramId', updateADrinkMiddleware, updateADrinkController);

// Xử lý khi người dùng muốn xóa 1 loại nước uống theo id truyền vào
drinkRouter.delete('/:paramId', deleteADrinkMiddleware, deleteADrinkController)

export { drinkRouter };