import express from "express";
// IMPORT MIDDLEWARES
import {
    getAllUserMiddleware,
    getAUserByIdMiddleware,
    createAUserMiddleware,
    updateAUserByIdMiddleware,
    deleteAUserByIdMiddleware,
    getLimitUserMiddleware,
    getSkipUserMiddleware,
    getSortesUsersMiddleware,
    getSkipLimitUsersMiddleware,
    getSortedSkipLimitUsersMiddleware
} from '../middlewares/userMiddleware.js';
// IMPORT MIDDLEWARES

// IMPORT CONTROLLER
import {
    getAllUserController,
    getAUserByIdController,
    createAUserController,
    updateAUserByIdController,
    deleteAUserByIdController,
    getLimitUserController,
    getSkipUserController,
    getSortesUsersController,
    getSkipLimitUsersController,
    getSortedSkipLimitUsersController
} from '../controller/userController.js'
// IMPORT CONTROLLER

// khởi tạo user router
const userRouter = express.Router();

// router xử lý lấy ra toàn bộ users
userRouter.get('/users', getAllUserMiddleware, getAllUserController);

// router xử lý lấy ra 1 user theo id truyền vào url dưới dạng param
userRouter.get('/users/:userId', getAUserByIdMiddleware, getAUserByIdController);

// router xử lý tạo mới 1 user
userRouter.post('/users', createAUserMiddleware, createAUserController);

// Router xử lý chỉnh sửa 1 user theo id vào body truyền vào
userRouter.put('/users/:userId', updateAUserByIdMiddleware, updateAUserByIdController);

// Router xử lý xóa 1 user theo id truyền vào dưới dạng param
userRouter.delete('/users/:userId', deleteAUserByIdMiddleware, deleteAUserByIdController)

// Router lấy dữ liệu limit từ request.query. Nếu limit có giá trị thì response trả về danh sách có limit số users, ngược lại nếu limit không có giá trị thì response sẽ trả về toàn bộ user. Response trả ra ở dạng JSON
userRouter.get('/limit-users', getLimitUserMiddleware, getLimitUserController);

// Router lấy dữ liệu skip từ request.query. Nếu skip có giá trị thì response trả về danh sách bỏ qua skip users đầu tiên, ngược lại nếu skip không có giá trị thì response sẽ trả về toàn bộ user. Response trả ra ở dạng JSON.
userRouter.get('/skip-users', getSkipUserMiddleware, getSkipUserController);

// Router get all user sorted (“/sort-users”) response sẽ trả về toàn bộ user được sắp xếp fullname theo thứ tự Alphabet. Response trả ra ở dạng JSON.
userRouter.get('/sort-users', getSortesUsersMiddleware, getSortesUsersController);

// Router get all user skip (“/skip-limit-users”), giống yêu cầu test 2, 3 để trả limit phần tử đầu tiên tính từ vị trí skip
userRouter.get('/skip-limit-users', getSkipLimitUsersMiddleware, getSkipLimitUsersController);

// router get all user sorted (“/sort-skip-limit-users”) response sẽ trả về toàn bộ user được sắp xếp fullname theo thứ tự Alphabet. Kết quả tìm được lấy limit phần tử đầu tiên tính từ vị trí skip. Response trả ra ở dạng JSON.
userRouter.get('/sort-skip-limit-users', getSortedSkipLimitUsersMiddleware, getSortedSkipLimitUsersController)

export { userRouter };