$(document).ready(() => {
    const gOrderTableCols = ['orderCode', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien', 'hoTen', 'soDienThoai', 'trangThai', 'action']; // Các cột dữ liệu trong DataTable

    let gDrinkList = []; // Biến danh sách nước uống toàn cục
    let vCurrOrderId = null; // Id của order hiện tại đang nhấn xem chi tiết -> dùng để update confirm or cancel

    onPageLoading(); // Gọi hàm thực hiện khi load trang

    // xử lý khi nhấn nút chi tiết ở mỗi dòng
    $(document).on('click', '.detail-btn', (e) => {
        let vCurrOrderData = $('#order-table').DataTable().row($(e.target).closest('tr')).data(); // Lấy ra được data của order tại dòng được nhấn nút chi tiết
        callApiToGetOrderByOrderCode(vCurrOrderData); // Call API để lấy order theo ordercode lấy ra được từ dữ liệu dòng hiện tại
        handleModal(vCurrOrderData); // Xử lý modal khi call API lấy về order theo ordercode thành công
    });

    // Xử lý khi nhấn nút confirm đơn hàng
    $('#confirm-order-btn').on('click', () => {
        callApiToUpdateOrder(vCurrOrderId, 'confirmed'); // thực hiện call API để update trạng thái confirmed
        handleWhenUpdateOrderSuccess(); // thực hiện hàm call API để lấy  lại danh sách order đã cập nhật và load lại data cho table
    });

    // Xử lý khi nhấn nút cancel đơn hàng
    $('#cancel-order-btn').on('click', () => {
        callApiToUpdateOrder(vCurrOrderId, 'cancel'); // thực hiện call API để update trạng thái cancel
        handleWhenUpdateOrderSuccess(); // thực hiện hàm call API để lấy  lại danh sách order đã cập nhật và load lại data cho table
    });

    // Xử lý khi load trang
    function onPageLoading() {
        callApiToGetDataDrinkList(); // call API để lấy danh sách nước uống
        createTable() // Tạo bảng bằng datatable
        callApiToGetAllOrders() // call API để lấy dữ liệu danh sách các order
    };

    // Xử lý khi lấy danh sách order thành công
    function handleGetAllOrdersSuccess(paramRe) {
        console.log(`%cLấy danh sách order thành công\n${JSON.stringify(paramRe)}`, 'color:green');
        loadDataIntoTable(paramRe); // Đổ dữ liệu lấy về được (danh sách các orders) vào bảng đã tạo
    };

    // Xử lý khi lấy danh sách order thất bại
    function handleGetAllOrdersError(paramErr) {
        console.log(`%cLấy danh sách order thất bại\n${JSON.stringify(paramErr)}`, 'color:red');
    };

    // call API để lấy danh sách orders
    function callApiToGetAllOrders() {
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            success: (re) => {
                handleGetAllOrdersSuccess(re);
            },
            error: (err) => {
                handleGetAllOrdersError(err);
            }
        })
    };

    // Xử lý khi lấy danh sách nước uống thành công
    function handleGetDataDrinkListSuccess(paramRe) {
        console.log(`%cLấy danh sách nước uống thành công!\n${JSON.stringify(paramRe)}`, 'color:green');
        gDrinkList = paramRe;
        loadSizePizzaIntoSelectElement(paramRe);
    };

    // Xử lý khi lấy danh sách nước uống thất bại
    function handleGetDataDrinkListError(paramErr) {
        console.log(`%cLấy danh sách nước uống thất bại!\n${JSON.stringify(paramErr)}`, 'color:red');
    };

    // Call API để lấy danh sách nước uống
    function callApiToGetDataDrinkList() {
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            async: 'true',
            url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
            success: (re) => {
                handleGetDataDrinkListSuccess(re);
            },
            error: (err) => {
                handleGetDataDrinkListError(err);
            }
        })
    };

    // Xử lý khi update order thành công
    function handleUpdateOrderSuccess(paramRe) {
        console.log(`%cUpdate thành công!\n${JSON.stringify(paramRe)}`, 'color:green');

    };

    // Xử lý khi update order thất bại
    function handleUpdateOrderError(paramErr) {
        console.log(`%cUpdate thất bại!\n${JSON.stringify(paramErr)}`, 'color:red');
    };

    // Call API để update order theo ID(đã lưu vào biến toàn cục lúc lấy dữ liệu order bằng ordercode thành công -> trạng thái cập nhật tùy vào tham số thứ 2 truyền vào (string: confirmed || cancel))
    function callApiToUpdateOrder(paramCurrOrderId, paramStatus) {
        $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            data: JSON.stringify({
                trangThai: paramStatus
            }),
            url: `http://203.171.20.210:8080/devcamp-pizza365/orders/${paramCurrOrderId}`,
            success: (re) => {
                handleUpdateOrderSuccess(re);
            },
            error: (err) => {
                handleUpdateOrderError(err);
            }
        });
    };

    // Hàm tạo bảng bằng datatable
    function createTable() {
        $('#order-table').DataTable({
            columns: [
                {
                    data: gOrderTableCols[0],
                },
                {
                    data: gOrderTableCols[1],
                },
                {
                    data: gOrderTableCols[2],
                },
                {
                    data: gOrderTableCols[3],
                },
                {
                    data: gOrderTableCols[4],
                },
                {
                    data: gOrderTableCols[5],
                },
                {
                    data: gOrderTableCols[6],
                },
                {
                    data: gOrderTableCols[7],
                },
                {
                    data: gOrderTableCols[8],
                }
            ],
            columnDefs: [
                {
                    targets: 0,
                    className: 'text-center align-middle'
                },
                {
                    targets: 1,
                    className: 'text-center align-middle'
                },
                {
                    targets: 2,
                    className: 'text-center align-middle'
                },
                {
                    targets: 3,
                    className: 'text-center align-middle',
                    render: renderDrink // render ra tên nước uống từ id nước uống
                },
                {
                    targets: 4,
                    className: 'text-center align-middle',
                },
                {
                    targets: 5,
                    className: 'text-center align-middle'
                },
                {
                    targets: 6,
                    className: 'text-center align-middle'
                },
                {
                    targets: 7,
                    className: 'text-center align-middle'
                },
                {
                    targets: 8,
                    defaultContent: `<tr>
                    <td class="row text-center align-middle d-flex justify-content-center">
                        <button class="col-md-10 mx-1 text-center btn btn-primary detail-btn">Chi&nbsp;tiết</button>
                    </td>
                </tr>`
                }
            ]
        })
    };

    // Hàm đổ dữ liệu vào bảng
    function loadDataIntoTable(paramOrderList) {
        let vTable = $('#order-table').DataTable();
        vTable.clear(); // Xóa dữ liệu cũ trong bảng
        vTable.rows.add(paramOrderList); // đổ dữ liệu vào bảng
        vTable.draw(); // xuất dữ liệu
    };

    // đổ tên nước uống vào cột tương ứng với id nước uống
    function renderDrink(paramDrinkId) {
        let vCheck = false;
        let vIndex = 0;
        let vResult = null;
        while (vCheck === false && vIndex < gDrinkList.length) {
            if (paramDrinkId === gDrinkList[vIndex].maNuocUong) {
                vResult = gDrinkList[vIndex].tenNuocUong;
                vCheck = true;
            } else {
                ++vIndex;
            };
        };
        return vResult;
    };

    // đổ các option size pizza vào element select size pizza
    function loadSizePizzaIntoSelectElement(paramDrinkList) {
        paramDrinkList.forEach(drink => {
            $('#drink-type-select-modal').append(`<option value=${drink['maNuocUong']}>${drink['tenNuocUong']}</option>`);
        });
    };

    // Xử lý khi lấy order bằng ordercode thành công
    function handleGetOrderByOrderCodeSuccess(paramRe) {
        console.log(`%clấy dữ liệu order bằng ordercode thành công!\n${JSON.stringify(paramRe)}`, 'color:green');
        vCurrOrderId = paramRe['id'];
    };

    // Xử lý khi lấy order bằng ordercode thất bại
    function handleGetOrderByOrderCodeError(paramErr) {
        console.log(`%clấy dữ liệu order bằng ordercode thất bại!\n${JSON.stringify(paramErr)}`, 'color:red');
    };

    // Call API để lấy order bằng ordercode (ordercode lấy từ dữ liệu tham số truyền vào)
    function callApiToGetOrderByOrderCode(paramOrderData) {
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            url: `http://203.171.20.210:8080/devcamp-pizza365/orders/${paramOrderData['orderCode']}`,
            success: (re) => {
                handleGetOrderByOrderCodeSuccess(re);
            },
            error: (err) => {
                handleGetOrderByOrderCodeError(err);
            }
        })
    };

    // hàm xử lý modal
    function handleModal(paramCurrData) {
        $('#order-detai-modal').modal('show');
        loadDataIntoModalBody(paramCurrData);
    };

    // Đổ dữ liệu của order hiện tại đang được nhấn nút chi tiết
    function loadDataIntoModalBody(paramCurrData) {
        $('#ordercode-inp-modal').val(paramCurrData.orderCode);
        $('#diameter-inp-modal').val(paramCurrData.duongKinh);
        $('#porks-inp-modal').val(paramCurrData.suon);
        $('#salad-inp-modal').val(paramCurrData.salad);
        $('#pizza-type-inp-modal').val(paramCurrData.loaiPizza);
        $('#id-voucher-inp-modal').val(paramCurrData.idVourcher);
        $('#price-inp-modal').val(paramCurrData.thanhTien);
        $('#dicount-price-inp-modal').val(paramCurrData.giamGia);
        $('#drink-quantity-inp-modal').val(paramCurrData.soLuongNuoc);
        $('#fullname-inp-modal').val(paramCurrData.hoTen);
        $('#email-inp-modal').val(paramCurrData.email);
        $('#mobile-inp-modal').val(paramCurrData.soDienThoai);
        $('#address-inp-modal').val(paramCurrData.diaChi);
        $('#message-inp-modal').val(paramCurrData.loiNhan);
        $('#order-date-inp-modal').val(paramCurrData.ngayTao);
        $('#fix-date-inp-modal').val(paramCurrData.ngayCapNhat);

        pickCurrSizePizzaOption(paramCurrData); // đưa ra đúng option size pizza được chọn của order hiện tại
        pickCurrDrinkOption(paramCurrData); // đưa ra đúng option nước uống được chọn hiện tại của order
    };

    // Hàm đưa ra đúng option size pizza được chọn của order hiện tại
    function pickCurrSizePizzaOption(paramCurrData) {
        let vSizePizzaSelectElm = $('#pizza-size-select-modal').children();
        let vCheck = false;
        let vIndex = 0;
        while (vCheck === false && vIndex < vSizePizzaSelectElm.length) {
            if (paramCurrData['kichCo'].toLowerCase() === vSizePizzaSelectElm[vIndex].value) {
                vSizePizzaSelectElm[vIndex].selected = true;
                vCheck = true;
            } else {
                ++vIndex;
            };
        };
    };

    // Hàm đưa ra đúng option nước uống được chọn hiện tại của order
    function pickCurrDrinkOption(paramCurrData) {
        let vDrinkSelectElm = $('#drink-type-select-modal').children();
        let vCheck = false;
        let vIndex = 0;
        while (vCheck === false && vIndex < vDrinkSelectElm.length) {
            if (paramCurrData['idLoaiNuocUong'].toLowerCase() === vDrinkSelectElm[vIndex].value.toLowerCase()) {
                vDrinkSelectElm[vIndex].selected = true;
                vCheck = true;
            } else {
                ++vIndex;
            };
        };
    };

    // Xử lý khi update order thành công
    function handleWhenUpdateOrderSuccess() {
        $('#order-detai-modal').modal('hide'); // Ẩn đi modal
        callApiToGetAllOrders(); // thực hiện hàm call API để lấy lại danh sách order đã cập nhật và load lại data cho table
    };

});