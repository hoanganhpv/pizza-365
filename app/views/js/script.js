$(document).ready(() => {
    // Khai báo lớp đối tượng kích cỡ pizza
    class PizzaSize {
        constructor(size, diameter, porks, salad, drink, price) {
            this.size = size;
            this.diameter = diameter;
            this.porks = porks;
            this.salad = salad;
            this.drink = drink;
            this.price = price;
        };
    };

    const gSSize = new PizzaSize('S', '20cm', 2, '200g', 2, 150000);
    const gMSize = new PizzaSize('M', '25cm', 4, '300g', 3, 200000);
    const gLSize = new PizzaSize('L', '30cm', 8, '500g', 4, 250000);

    let gSizePizzaTargeting = null; // Biến size pizza đang nhấn toàn cục
    let gTypePizzaTargeting = null; // Biến type pizza đang nhấn toàn cục
    let gDrinkTargeting = null; // Biến drink đang chọn toàn cục
    let gUserData = null; // Biến thông tin đơn hàng toàn cục

    onPageLoading();

    // Nhấn nút chọn size pizza => lấy ra được data của card đó và lưu vào biến toàn cục -> đổi màu title của card
    $('.size-card-btn').on('click', (e) => {
        $('.card-bd-top').removeClass('card-clicked'); // Xóa bỏ class thay đổi màu ở các card khác
        let vCurrCardClicking = $(e.target).parents('.card'); //Card hiện tại đang được nhấn => phần tử Jquery
        gSizePizzaTargeting = vCurrCardClicking.data('size'); // Data của size card hiện tại đang nhấn
        console.log(`${JSON.stringify(gSizePizzaTargeting)}\n`); //In data của card hiện tại ra console
        vCurrCardClicking.children('.card-bd-top').addClass('card-clicked'); // thêm class để thay đổi màu của card title hiện tại được nhấn
    });

    // Nhấn nút chọn type pizza => lấy ra được loại pizza đang chọn và lưu vào biến toàn cục
    $('.type-card-btn').on('click', (e) => {
        let vCurrCardClicking = $(e.target).parents('.card'); // Card hiện tại đang được nhấn => phần tử Jquery
        gTypePizzaTargeting = vCurrCardClicking.data('type'); // Data của type card hiện tại đang nhấn
        console.log(`${gTypePizzaTargeting}\n`);
    });

    // Nhấn nút Gửi đơn -> hiện lên modal tạo đơn
    $(document).on('click', '#send-order-btn', (e) => {
        e.preventDefault();
        if (!validateData()) {
            return;
        } else {
            handleWhenValidateSuccess();
        };
    });

    // Nhấn nút quay lại
    $('#create-order-comback-btn').on('click', () => {
        turnBack();
    });

    // Nhấn nút tạo đơn hàng
    $('#create-order-btn').on('click', () => {
        callApiToCreateOrder();
    });

    // function thực hiện khi load trang:
    function onPageLoading() {
        loadSizePizzaDataIntoCardElement(); // Đổ dữ liệu size pizza vào từng card tương ứng
        loadTypePizzaDataIntoCardElement(); // Đổ dữ liệu type pizza vào từng card tương ứng
        callApiToGetDrinklist(); // Call API lấy danh sách nước uống và đổ các option nước uống vào drink select
    };

    //Hàm đổ dữ liệu từng size pizza vào card tương ứng
    function loadSizePizzaDataIntoCardElement() {
        $('#s-size').data('size', gSSize);
        $('#m-size').data('size', gMSize);
        $('#l-size').data('size', gLSize);
    };

    // Hàm đổ dữ liệu từng type pizza vào card tương ứng
    function loadTypePizzaDataIntoCardElement() {
        $('#ocean').data('type', 'ocean mania');
        $('#hawaii').data('type', 'hawaiian');
        $('#chicken').data('type', 'chessy chicken bacon');
    };

    // Thực hiện khi call API lấy danh sách nước uống thành công
    function handleCallApiToGetDrinkListSuccess(paramRe) {
        console.log(`%cGet drink list successful!\n${JSON.stringify(paramRe)}\n`, 'color:green');
        loadDrinkOptionListIntoDrinkSelect(paramRe); // Đổ các option nước uống vào drink select
    };

    // Thực hiện khi call API lấy danh sách nước uống thất bại
    function handleCallApiToGetDrinkListError(paramErr) {
        console.log(`%cGet drink list failed!\n${JSON.stringify(paramErr)}\n`, 'color:red');
    };

    // Call API lấy danh sách nước uống
    function callApiToGetDrinklist() {
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
            success: (re) => {
                handleCallApiToGetDrinkListSuccess(re);
            },
            error: (err) => {
                handleCallApiToGetDrinkListError(err);
            }
        });
    };

    // Xử lú khi check voucher thành công -> có voucher
    function handleCheckVoucherSuccess(paramRe, paramDiscountPercent, paramUserData) {
        console.log(`%cMã voucher tồn tại\n${JSON.stringify(paramRe)}`, 'color:green');
        paramDiscountPercent = paramRe['phanTramGiamGia'];
        paramUserData.discountPercent = paramDiscountPercent;
        let vTotalPrice = paramUserData.size.thanhTien - (paramUserData.size.thanhTien * paramUserData.discountPercent / 100);
        handleSendOrderModal(paramUserData, vTotalPrice); // xử lý modal create order
    };

    // Xử lý khi check voucher thất bại -> voucherID sai
    function handleCheckVoucherError(paramErr, paramDiscountPercent, paramUserData) {
        console.log(`%cMã voucher không tồn tại\n${JSON.stringify(paramErr)}`);
        paramUserData.discountPercent = paramDiscountPercent;
        handleSendOrderModal(paramUserData); // xử lý modal create order
    };

    // Call API để lấy discount %
    function callApiToCheckDiscountVoucher(paramUserData) {
        let vDiscountPercent = 0;
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            url: `http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/${paramUserData['idVourcher']}`,
            success: (re) => {
                handleCheckVoucherSuccess(re, vDiscountPercent, paramUserData);
            },
            error: (err) => {
                handleCheckVoucherError(err, vDiscountPercent, paramUserData);
            }
        });
    };

    // Xử lý khi tạo order mới thành công
    function handleCreateOrderSuccess(paramRe) {
        console.log(`%Tạo order mới thành công!\n${JSON.stringify(paramRe)}`, 'color:green');
        $('#alert-success-modal').modal('show');
        $('#alert-ordercode-success').html(paramRe['orderCode']);
        resetUserForm();
    };

    // Xử lý khi tạo order mới thất bại
    function handleCreateOrderError(paramErr) {
        console.log(`%cTạo order mới thất bại!\n${JSON.stringify(paramErr)}`, 'color:red');
    };

    // gọi API để tạo mới order dựa trên biên userdata toàn cục
    function callApiToCreateOrder() {
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(gUserData),
            dataType: 'json',
            async: true,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            success: (re) => {
                handleCreateOrderSuccess(re);
            },
            error: (err) => {
                handleCreateOrderError(err);
            }
        });
    };

    // Đổ các option nước uống vào drink select
    function loadDrinkOptionListIntoDrinkSelect(paramDrinkListGeted) {
        paramDrinkListGeted.forEach(drink => {
            $('#drink-select').append(`<option value=${drink['maNuocUong']}>${drink['tenNuocUong']}</option>`);
        });
    };

    // Hàm validate từng bước
    function validateData() {
        let vCheck = false;
        if (!checkExistSizeData() || !checkExistTypeData() || !checkExistDrinkValue()) {
            vCheck = false;
        } else {
            if (!$('form').valid()) {
                vCheck = false;
            } else {
                vCheck = true;
                console.log(`%cValidate data success!\n`, 'color:green');
            }
        };
        return vCheck;
    };

    // Kiểm tra xem đã chọn size pizza hay chưa
    function checkExistSizeData() {
        if (gSizePizzaTargeting === null) {
            alert('Chưa chọn kích cỡ pizza!');
            return false;
        } else {
            return true;
        };
    };

    // Kiểm tra xem đã chọn loại pizza hay chưa
    function checkExistTypeData() {
        if (gTypePizzaTargeting === null) {
            alert('Chưa chọn loại pizza!');
            return false;
        } else {
            return true;
        };
    };

    // Kiểm tra đã chọn nước uống hay chưa
    function checkExistDrinkValue() {
        gDrinkTargeting = $('#drink-select').val();
        if (gDrinkTargeting === null || gDrinkTargeting === '0') {
            alert('Chưa chọn nước uống!');
            return false;
        } else {
            return true;
        };
    };

    // Hàm kiếm tra (validate) dữ liệu người dùng nhập vào form order
    $('form').validate({
        rules: {
            fullname: {
                required: true,
            },
            email: {
                required: true,
                regex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
            },
            mobile: {
                required: true,
                regex: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
            },
            address: {
                required: true
            }
        },
        messages: {
            fullname: {
                required: 'Vui lòng điền họ và tên'
            },
            email: {
                required: 'Vui lòng nhập email',
                regex: 'Vui lòng nhập đúng định dạng email'
            },
            mobile: {
                required: 'Vui lòng nhập số điện thoại',
                regex: 'Vui lòng nhập đúng định dạng số điện thoại'
            },
            address: {
                required: 'Vui lòng nhập địa chỉ'
            }
        }
    });

    // Thêm method kiểm tra bằng regex
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );

    // Hàm trả về obj bao gồm các thông tin người dùng + size pizza + type pizza người dùng đã chọn
    function getOrderData() {
        return {
            hoTen: $('#fullname-inp').val(),
            email: $('#email-inp').val(),
            soDienThoai: $('#mobile-inp').val(),
            diaChi: $('#address-inp').val(),
            idVourcher: $('#voucher-inp').val(),
            loiNhan: $('#messages-inp').val(),
            size: {
                kichCo: gSizePizzaTargeting['size'],
                duongKinh: gSizePizzaTargeting['diameter'],
                suon: gSizePizzaTargeting['porks'],
                salad: gSizePizzaTargeting['salad'],
                soLuongNuoc: gSizePizzaTargeting['drink'],
                thanhTien: gSizePizzaTargeting['price']
            },
            type: gTypePizzaTargeting,
            idLoaiNuocUong: gDrinkTargeting,
            discountPercent: 0
        };
    };

    // Thực hiện khi validate tất cả đều pass
    function handleWhenValidateSuccess() {
        let vUserOrderDataGeted = getOrderData();
        callApiToCheckDiscountVoucher(vUserOrderDataGeted);
        console.log(`%cGet user order success!\n user data:${JSON.stringify(vUserOrderDataGeted)}\n`, 'color:green');
    };

    // hàm xử lý modal create order khi validate success
    function handleSendOrderModal(paramUserData, paramTotal) {
        $('#create-order-modal').modal('show');
        $('#create-order-modal').data('order', paramUserData);
        gUserData = {
            kichCo: paramUserData.size.kichCo,
            duongKinh: paramUserData.size.duongKinh,
            suon: paramUserData.size.suon,
            salad: paramUserData.size.salad,
            loaiPizza: paramUserData.type,
            idVourcher: paramUserData.idVourcher,
            idLoaiNuocUong: paramUserData.idLoaiNuocUong,
            soLuongNuoc: paramUserData.size.soLuongNuoc,
            hoTen: paramUserData.hoTen,
            thanhTien: paramUserData.size.thanhTien,
            email: paramUserData.email,
            soDienThoai: paramUserData.soDienThoai,
            diaChi: paramUserData.diaChi,
            loiNhan: paramUserData.loiNhan
        };

        $('#fullname-modal').val(paramUserData['hoTen']);
        $('#mobile-modal').val(paramUserData['soDienThoai']);
        $('#address-modal').val(paramUserData['diaChi']);
        $('#messages-modal').val(paramUserData['loiNhan']);
        $('#voucher-modal').val(paramUserData['idVourcher']);
        $('#detail-modal').val(`Xác nhận: ${paramUserData['hoTen']}, ${paramUserData['soDienThoai']}, ${paramUserData['diaChi']}\r\nMenu ${paramUserData.size.kichCo}, đường kính ${paramUserData.size.duongKinh}, nước ${paramUserData.size.soLuongNuoc}, salad ${paramUserData.size.salad}\r\nLoại Pizza: ${paramUserData.type}, Giá ${paramUserData.size.thanhTien} vnd, Mã giảm giá: ${paramUserData.idVourcher}\r\nPhải thanh toán: ${paramTotal ? paramTotal : paramUserData.size.thanhTien} vnd (giảm giá ${paramUserData.discountPercent}%)\r\n`);
    };

    // quay lại khi nhấn nút quay lại
    function turnBack() {
        $('#create-order-modal').modal('hide');
        $('#fullname-modal').val('');
        $('#mobile-modal').val('');
        $('#address-modal').val('');
        $('#messages-modal').val('');
        $('#voucher-modal').val('')
        $('#detail-modal').val(``);
    };

    // Reset lại form nhập thông tin sau khi tạo mới order thành công
    function resetUserForm() {
        $('#fullname-inp').val('');
        $('#email-inp').val('');
        $('#mobile-inp').val('');
        $('#address-inp').val('');
        $('#voucher-inp').val('');
        $('#messages-inp').val('');
    };
});

