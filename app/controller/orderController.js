import mongoose from "mongoose";
import { Order } from "../models/orderModel.js";
import { User } from "../models/userModel.js";

// Tạo hàm thực hiện tạo mới 1 order thuộc 1 user đã truyền id vào trong url
const createAOrderOfUserController = async (req, res) => {
    try {
        const vUserId = req.params.userId;
        const { pizzaSize, pizzaType, status } = req.body;
        const vNewOrder = new Order({
            _id: new mongoose.Types.ObjectId(),
            pizzaSize: pizzaSize,
            pizzaType: pizzaType,
            status: status
        })
        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (!pizzaSize || typeof (pizzaSize) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (!pizzaType || typeof (pizzaType) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (!status || typeof (status) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vNewOrderCreateResult = await Order.create(vNewOrder);

        const vNewOrderResult = await User.findByIdAndUpdate(vUserId, {
            $push: { orders: vNewOrder._id }
        }, {
            new: true
        });

        res.status(201).json({
            result: vNewOrderResult.orders
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Tạo hàm thực hiện lấy ra toàn bộ order của 1 user
const getAllOrderByUserIdController = async (req, res) => {
    try {
        const vUserId = req.params.userId;

        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const allOrderResult = await User.findById(vUserId).populate('orders');

        if (allOrderResult.orders.length === 0) {
            return res.status(404).json({
                errorMessage: `Không tìm thấy order nào của user này!`
            });
        } else {
            res.status(200).json({
                orders: allOrderResult.orders
            });
        };

    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Hàm thực hiện lấy ra toàn bộ order có trong database
const getAllOrderController = async (req, res) => {
    try {
        const allOrderResult = await Order.find();

        if (allOrderResult.length === 0) {
            return res.status(404).json({
                message: `Hiện không có bất cứ order nào trong cơ sở dữ liệu!`
            });
        } else {
            res.status(200).json({
                orders: allOrderResult
            });
        };
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Hàm thực hiện lấy ra 1 order chỉ định thuộc 1 user chỉ định (hàm này hơi thừa, làm cho vui, train trình)
const getAOrderOfUserController = async (req, res) => {
    try {
        const vUserId = req.params.userId;
        const vOrderId = req.params.orderId;

        if (!mongoose.Types.ObjectId.isValid(vUserId) || !mongoose.Types.ObjectId.isValid(vOrderId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vUserResult = await User.findById(vUserId);

        const vOrderResult = await vUserResult.orders.find(order => order._id.toString() === vOrderId);

        if (!vOrderResult) {
            return res.status(404).json({
                errorMessage: `Không tìm thấy order này!`
            })
        } else {
            res.status(200).json({
                result: vOrderResult
            });
        };
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Hàm thực hiện cập nhật 1 order 
const updateAOrderController = async (req, res) => {
    try {
        const vOrderId = req.params.orderId;
        const { pizzaSize, pizzaType, status } = req.body;
        const vNewOrderData = new Order({
            pizzaSize: pizzaSize,
            pizzaType: pizzaType,
            status: status
        })
        if (!mongoose.Types.ObjectId.isValid(vOrderId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (pizzaSize && typeof (pizzaSize) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (pizzaType && typeof (pizzaType) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (status && typeof (status) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (!pizzaSize && !pizzaType && !status) {
            return res.status(400).json({
                errorMessage: `Không có dữ liệu nào cần cập nhật!`
            });
        };

        const vOrderUpdateResult = await Order.findByIdAndUpdate(vOrderId, vNewOrderData);
        res.status(200).json({
            result: `Update OK!`,
            newOrderData: vOrderUpdateResult
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Hàm thực hiện xử lý xóa 1 order chỉ định
const deleteAOrderController = async (req, res) => {
    try {
        const vOrderId = req.params.orderId;

        if (!mongoose.Types.ObjectId.isValid(vOrderId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vDeleteOrderResult = await Order.findOneAndDelete({
            _id: vOrderId
        });

        res.status(204).json({
            none: "none"
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

export {
    createAOrderOfUserController,
    getAllOrderByUserIdController,
    getAllOrderController,
    getAOrderOfUserController,
    updateAOrderController,
    deleteAOrderController
};