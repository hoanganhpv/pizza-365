// import module mongoose
import mongoose from "mongoose";
// impoprt model drink
import { Drink } from '../models/drinkModel.js';

// Khai báo hàm thực hiện lấy ra toàn bộ danh sách nước uống từ database
const getAllDrinkController = async (req, res) => {
    try {
        const allDrinks = await Drink.find(); // Lấy ra danh sách drink từ database
        res.status(200).json({
            drinks: allDrinks
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error get all drinks`
        })
    }
};

// Khai báo hàm thực hiện tạo mới 1 loại nước uống
const createAdrinkController = async (req, res) => {
    try {
        const { maNuocUong, tenNuocUong, donGia } = req.body;
        const newDrink = new Drink({
            _id: new mongoose.Types.ObjectId(),
            maNuocUong: maNuocUong,
            tenNuocUong: tenNuocUong,
            donGia: donGia
        });

        // Validate dữ liệu
        if (!maNuocUong) {
            return res.status(400).json({
                errorMessage: `Client chưa nhập maNuocUong`
            });
        };

        if (typeof (maNuocUong) != 'string') {
            return res.status(400).json({
                errorMessage: `maNuocUong phải có định dạng là string`
            });
        };

        if (!tenNuocUong) {
            return res.status(400).json({
                errorMessage: `Client chưa nhập tenNuocUong`
            });
        };

        if (typeof (tenNuocUong) != 'string') {
            return res.status(400).json({
                errorMessage: `tenNuocUong phải có định dạng là string`
            });
        };

        if (donGia) {
            if (typeof (donGia) != 'number') {
                return res.status(400).json({
                    errorMessage: `donGia phải có định dạng là number`
                });
            };
        }


        await Drink.create(newDrink); // Tiến hành thực hiện phương thức tạo mới nước uống
        res.status(201).json({
            message: {
                response: `Đã tạo thành công loại nước uống mới`
            }
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            errorMessage: `Internal server error`
        });
    };
};

// Khai báo hàm thực hiện cập nhật 1 loại nước uống
const updateADrinkController = async (req, res) => {
    try {
        const vId = req.params.paramId;
        const { maNuocUong, tenNuocUong, donGia } = req.body;
        const newData = new Drink({
            maNuocUong: maNuocUong,
            tenNuocUong: tenNuocUong,
            donGia: donGia
        });
        if (!mongoose.Types.ObjectId.isValid(vId)) {
            return res.status(400).json({ errorMessage: `Id không đúng!` });
        } else if (maNuocUong && typeof (maNuocUong) !== 'string') {
            return res.status(400).json({ errorMessage: `maNuocUong phải có định dạng là string` });
        } else if (tenNuocUong && typeof (tenNuocUong) !== 'string') {
            return res.status(400).json({ errorMessage: `tenNuocUong phải có định dạng là string` });
        } else if (donGia && typeof (donGia) !== 'number') {
            res.status(400).json({
                errorMessage: `donGia phải có định dạng là number`
            });
        } else {
            await Drink.findByIdAndUpdate(vId, newData);
            return res.status(201).json({ message: `Cập nhật nước uống thành công!` });
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    }
};

// Khai báo hàm thực hiện xóa 1 loại nước uống
const deleteADrinkController = async (req, res) => {
    try {
        const vId = req.params.paramId
        if (!mongoose.Types.ObjectId.isValid(vId)) {
            return res.status(400).json({
                errorMessage: `Id không hợp lệ!`
            });
        } else {
            const result = await Drink.findOneAndDelete({ _id: vId });
            if (!result) {
                res.status(404).json({
                    errorMessage: `Id này đã không còn trong cở sở dữ liệu!`
                })
            } else {
                res.status(200).json({
                    message: `Đã xóa thành công nước uống theo id truyền vào!`
                });
            };
        };

    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

export {
    getAllDrinkController,
    createAdrinkController,
    updateADrinkController,
    deleteADrinkController
};