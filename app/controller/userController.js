import mongoose from "mongoose";
import { User } from "../models/userModel.js";

// Tạo hàm controller lấy ra toàn  bộ user
const getAllUserController = async (req, res) => {
    try {
        const allUser = await User.find();
        res.status(200).json({
            users: allUser
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Tạo hàm controller xử lý lấy ra 1 user theo id truyền vào
const getAUserByIdController = async (req, res) => {
    try {
        const vUserId = req.params.userId
        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vUserGeted = await User.findById(vUserId);
        res.status(200).json({
            user: vUserGeted
        });
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Tạo hàm controller xử lý tạo mới 1 user
const createAUserController = async (req, res) => {
    try {
        const { fullName, email, address, phone } = req.body;
        const vNewUser = new User({
            _id: new mongoose.Types.ObjectId(),
            fullName: fullName,
            email: email,
            address: address,
            phone: phone,
        });

        if (!fullName || typeof (fullName) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (!email || typeof (email) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (!address || typeof (address) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (!phone || typeof (phone) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vNewUserResult = await User.create(vNewUser);
        res.status(201).json({
            user: vNewUserResult
        });
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Tạo hàm xử lý chỉnh sửa, cập nhật 1 user theo id và body truyền vào.
const updateAUserByIdController = async (req, res) => {
    try {
        const vUserId = req.params.userId;
        const { fullName, email, address, phone } = req.body;
        const vNewUserData = {
            fullName: fullName,
            email: email,
            address: address,
            phone: phone,
        };

        if (!mongoose.Types.ObjectId.isValid(vUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        }

        if (fullName && typeof (fullName) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (email && typeof (email) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (address && typeof (address) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        if (phone && typeof (phone) !== 'string') {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vUpdateResult = await User.findByIdAndUpdate(vUserId, vNewUserData);
        res.status(200).json({
            result: vUpdateResult
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Tạo hàm xử lý xóa 1 user theo id truyền vào dưới dạng param
const deleteAUserByIdController = async (req, res) => {
    try {
        const vDeleteUserId = req.params.userId;
        if (!mongoose.Types.ObjectId.isValid(vDeleteUserId)) {
            return res.status(400).json({
                errorMessage: `Bad request!`
            });
        };

        const vDeleteResult = await User.findOneAndDelete({ _id: vDeleteUserId });
        if (!vDeleteResult) {
            res.status(404).json({
                errorMessage: `User này đã không còn tồn tại trong cơ sở dữ liệu!`
            })
        } else {
            res.status(204).json({
                none: 'none'
            });
        };
    } catch (err) {
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    };
};

// Hàm lấy ra 1 số lượng user nhất định theo query truyền vào: Nếu limit có giá trị thì response trả về danh sách có limit số users, ngược lại nếu limit không có giá trị thì response sẽ trả về toàn bộ user. Response trả ra ở dạng JSON
const getLimitUserController = async (req, res) => {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
        const vLimitNumber = req.query.limit;

        let vGetUser = null;

        if (!vLimitNumber) {
            vGetUser = await User.find().sort({ _id: 'desc' });
            res.status(200).json({
                result: vGetUser
            });

        } else if (vLimitNumber && typeof (parseInt(vLimitNumber)) === 'number') {
            vGetUser = await User.find().sort({ _id: 'desc' }).limit(parseInt(vLimitNumber));

            res.status(200).json({
                result: vGetUser
            });
        } else {
            vGetUser = await User.find().sort({ _id: 'desc' });
            res.status(200).json({
                result: vGetUser
            });
        };
        session.commitTransaction();
    } catch (err) {
        console.log(err);
        session.abortTransaction();
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    } finally {
        session.endSession();
    };
};

// // function lấy dữ liệu skip từ request.query. Nếu skip có giá trị thì response trả về danh sách bỏ qua skip users đầu tiên, ngược lại nếu skip không có giá trị thì response sẽ trả về toàn bộ user. Response trả ra ở dạng JSON.
const getSkipUserController = async (req, res) => {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
        const vSkipNumber = req.query.skip;
        let vGetUser = null;

        if (!vSkipNumber) {
            vGetUser = await User.find().sort({ _id: 'desc' });
            res.status(200).json({
                result: vGetUser
            });
        } else if (vSkipNumber && typeof (parseInt(vSkipNumber)) === 'number') {
            vGetUser = await User.find().sort({ _id: 'desc' }).skip(parseInt(vSkipNumber));
            res.status(200).json({
                result: vGetUser
            });
        } else {
            vGetUser = await User.find().sort({ _id: 'desc' });
            res.status(200).json({
                result: vGetUser
            });
        };
        session.commitTransaction();
    } catch (err) {
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        });
    } finally {
        session.endSession();
    };
};

//  function get all user sorted (“/sort-users”) response sẽ trả về toàn bộ user được sắp xếp fullname theo thứ tự Alphabet. Response trả ra ở dạng JSON.
const getSortesUsersController = async (req, res) => {
    const sessison = await mongoose.startSession();
    sessison.startTransaction();
    try {
        const vResult = await User.find().sort({ fullName: 'asc' });;
        if (!vResult) {
            return res.status(404).json({
                errorMessage: `users 404 not found!`
            });
        };

        res.status(200).json({
            result: vResult
        });
        sessison.commitTransaction();
    } catch (err) {
        sessison.abortTransaction();
        console.log(err);
        res.status(500).json({
            errorMessage: `Internal server error!`
        })
    } finally {
        sessison.endSession();
    };
};

const getSkipLimitUsersController = async (req, res) => {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
        const vSkipNumber = req.query.skip;
        const vLimitNumber = req.query.limit;

        let vGetUserResult = null;

        if (vSkipNumber && vLimitNumber && typeof (parseInt(vSkipNumber)) !== 'number' && typeof (parseInt(vLimitNumber)) !== 'number') {
            vGetUserResult = await User.find();
        } else if (vSkipNumber && typeof (parseInt(vSkipNumber)) !== 'number' && !vLimitNumber) {
            vGetUserResult = await User.find();
        } else if (vLimitNumber && typeof (parseInt(vLimitNumber)) !== 'number' && !vSkipNumber) {
            vGetUserResult = await User.find();
        } else {
            vGetUserResult = await User.find().skip(parseInt(vSkipNumber)).limit(parseInt(vLimitNumber));
        };

        if (!vGetUserResult) {
            return res.status(400).json({
                errorMessage: `Bad resquest!`
            });
        };

        res.status(200).json({
            result: vGetUserResult
        });

        session.commitTransaction();
    } catch (err) {
        session.abortTransaction();
        console.log(err);
        errorMessage: `Internal server error!`
    } finally {
        session.endSession();
    };
};

const getSortedSkipLimitUsersController = async (req, res) => {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
        const vSkipNumber = req.query.skip;
        const vLimitNumber = req.query.limit;

        let vGetUserResult = null;

        if (vSkipNumber && vLimitNumber && typeof (parseInt(vSkipNumber)) !== 'number' && typeof (parseInt(vLimitNumber)) !== 'number') {
            vGetUserResult = await User.find().sort({ fullName: 'asc' });
        } else if (vSkipNumber && typeof (parseInt(vSkipNumber)) !== 'number' && !vLimitNumber) {
            vGetUserResult = await User.find().sort({ fullName: 'asc' });
        } else if (vLimitNumber && typeof (parseInt(vLimitNumber)) !== 'number' && !vSkipNumber) {
            vGetUserResult = await User.find().sort({ fullName: 'asc' });
        } else {
            vGetUserResult = await User.find().sort({ fullName: 'asc' }).skip(parseInt(vSkipNumber)).limit(parseInt(vLimitNumber));
        };

        if (!vGetUserResult) {
            return res.status(400).json({
                errorMessage: `Bad resquest!`
            });
        };

        res.status(200).json({
            result: vGetUserResult
        });

        session.commitTransaction();
    } catch (err) {

    } finally {
        session.endSession();
    }
}

// Xuất ra các hàm controller
export {
    getAllUserController,
    getAUserByIdController,
    createAUserController,
    updateAUserByIdController,
    deleteAUserByIdController,
    getLimitUserController,
    getSkipUserController,
    getSortesUsersController,
    getSkipLimitUsersController,
    getSortedSkipLimitUsersController
};