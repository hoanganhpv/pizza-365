// Khai báo module express
import express from "express";
// Khai báo module path
import path, { format } from "path";
// khai báo module fileURLToPath
import { fileURLToPath } from "url";
// lấy ra đường dẫn tới file hiện tại bằng module fileURLToPath
const __filename = fileURLToPath(import.meta.url);
// Khai báo module mongoose để thực hiện tương tác với dữ liệu tại mongoDB
import mongoose from "mongoose";
// khai báo module mime để thực hiện lấy được header của file trả về cho client
import mime from "mime-types";
/**IMPORT ROUTERS */
import { drinkRouter } from './app/routes/drinkRouter.js';
import { userRouter } from './app/routes/userRouter.js';
import { orderRouter } from './app/routes/orderRouter.js';
/**IMPORT ROUTERS */
/**iMPORT MODEL */
import { Voucher } from './app/models/voucherModel.js';
import { Drink } from './app/models/drinkModel.js';
import { Order } from './app/models/orderModel.js';
import { User } from './app/models/userModel.js';
/**IMPORT MODEL */

// Lấy ra đường dẫn tới thư mục chứa file hiện tại bằng phương thức dirname trong module path
const __dirname = path.dirname(__filename);

// Xử lý tiếp nhận và bắt lỗi khi kết nối tới cơ sở dữ liệu của Pizza-365
mongoose.connect('mongodb://127.0.0.1:27017/pizza-365')
    .then(() => {
        console.log(`Connect to Pizza-365 database Successfully!`);
    }).catch((err) => {
        throw err
    });

// Khởi tạo application (ứng dụng)
const app = express();

// Khao báo cổng hoạt động tại cổng 8000
const port = 8000;

// Cho phép ứng dụng sử dụng dữ liệu dạng JSON
app.use(express.json());

// Khi người dùng truy cập đến trang chủ, thực hiện 1 middlewares ở tầng application console.log về server để thông báo client đã truy cập vào server tại thời điểm nào
app.use('/', (req, res, next) => {
    let vCurrDate = new Date();
    console.log(`Client vừa truy cập đến trang chủ Pizza-365 vào lúc ${vCurrDate}`);
    next();
});

// Trả dữ liệu về cho user khi user truy cập đến trang chủ pizza 365
app.get('/', (req, res) => {
    const __publicDir = path.join(`${__dirname}/app/views/index.html`); // Đường dẫn của file html trang chủ
    const vMimeTypes = mime.lookup(__publicDir); // Dùng module mime-types trả về được headers của file html
    res.setHeader('Content-Type', vMimeTypes); // set Header cho file html
    res.status(200).sendFile(__publicDir); // Gửi trả file html về cho client
});

// Thiết lập tự động xác định được header cho mỗi file cần trả về client, sau đó set header cho nó và trả về client
app.use(express.static(path.join(`${__dirname}/app/views`), {
    setHeaders: (res, path, stat) => {
        res.set('Content-Type', mime.lookup(path));
    }
}));

// Thiết lập kết nối đường dẫn (route) tới router Drink
app.use('/drinks', drinkRouter) // Chuyển tiếp xử lý các tác vụ liên quan đến drinks
app.use('/api', userRouter); // chuyển tiếp tới router xử lý các tác vụ liên quan đến users
app.use('/api', orderRouter) // chuyển tiếp tới router xử lý các tác vụ liên quan đến order

// app listen on port
app.listen(port, () => {
    console.log(`Pizza-365 server is listening on port: ${port}`);
});